export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    __dangerouslyDisableSanitizers: ["script"],
    title: "qm-quiz",
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },

  target: "static",

  publicRuntimeConfig: {
    QUIZ_URL: process.env.QUIZ_URL,
    API_URL: process.env.API_URL,
    QUIZ_BUILDER_URL: process.env.QUIZ_BUILDER_URL,
    DASHBOARD_URL: process.env.DASHBOARD_URL
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~/assets/index.scss"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/axios",
    "~/plugins/vue-notification.client",
    "~/plugins/vue-notification.server",
    "~/plugins/vue-smooth-picker.client",
    "~/plugins/appsflyer.client",
    "~/plugins/appsflyer-links-generation.client",
    "~/plugins/vue-loaders.client",
    "~/plugins/v-tooltip.client"
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    "@nuxt/typescript-build",
    // https://go.nuxtjs.dev/stylelint
    "@nuxtjs/stylelint-module",
    // https://www.npmjs.com/package/@nuxtjs/style-resources
    "@nuxtjs/style-resources"
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    "@nuxtjs/i18n",
    "cookie-universal-nuxt",
    "@nuxtjs/dayjs",
    "@nuxtjs/robots",
    "@nuxtjs/sentry"
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.API_URL
  },

  i18n: {
    locales: [
      {
        code: "en",
        file: "en/en.js"
      }
    ],
    lazy: true,
    langDir: "locales",
    defaultLocale: "en"
  },

  styleResources: {
    scss: [
      "./assets/variables.scss",
      "./assets/typography.scss",
      "./assets/v-tooltip.scss"
    ]
  },

  robots: {
    UserAgent: "*",
    Disallow: "/"
  },

  sentry: {
    dsn: process.env.SENTRY_DSN,
    tracesSampleRate: 0.5,
    browserTracing: {},
    vueOptions: {
      trackComponents: true
    },
    config: {
      tracesSampleRate: 0.5,
      sampleRate: 1,
      beforeSend(event, hint) {
        if (
          hint &&
          hint.originalException &&
          hint.originalException.isAxiosError
        ) {
          if (
            hint.originalException.response &&
            hint.originalException.response.data
          ) {
            const contexts = {
              ...event.contexts
            };
            contexts.errorResponse = {
              data: hint.originalException.response.data
            };
            event.contexts = contexts;
          }
        }

        return event;
      },
      environment: process.env.DEV_MODE === "true" ? "dev" : "prod"
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build

  serverMiddleware: ["~/middleware/apple-pay"]
};
