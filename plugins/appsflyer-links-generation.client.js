import Vue from "vue";

Vue.mixin({
  methods: {
    getCookie(name) {
      const value = `; ${document.cookie}`;
      const parts = value.split(`; ${name}=`);
      if (parts.length === 2) return parts.pop().split(";").shift();
    },

    generateDeeplink(url) {
      const result = window.AF_SMART_SCRIPT.generateOneLinkURL({
        oneLinkURL: url,
        afParameters: {
          mediaSource: { keys: ["utm_source"], defaultValue: "no_utm" },
          campaign: { keys: ["utm_campaign"], defaultValue: "no_campaign" },
          ad: { keys: ["utm_ad"], defaultValue: "no_ad" },
          adSet: { keys: ["utm_adset"], defaultValue: "no_adset" },
          afSub1: { keys: ["fbclid"], defaultValue: "no_fbclid" },
          afSub2: { keys: ["fbc"], defaultValue: this.getCookie("_fbc") },
          afSub3: { keys: ["fbp"], defaultValue: this.getCookie("_fbp") },
          afSub4: { keys: ["ua"], defaultValue: window.navigator.userAgent },
          afSub5: {
            keys: ["source_url"],
            defaultValue: window.location.origin
          }
        },
        referrerSkipList: [],
        urlSkipList: []
      });

      if (result) {
        return result.clickURL;
      }

      return "";
    }
  }
});
