interface AnalyticsProps {
  ga_counter_id: string | null;
  ym_counter_id: number | null;
}

interface FbProps {
  fb_pixel_id: string | null;
  fb_token: string | null;
  fb_domain_verification: string | null;
}

export interface Quiz {
  json_data: object | Array<any> | null;
  fb: FbProps | null;
  analytics: AnalyticsProps | null;
}
