const https = require("https");

export default function (req, res, next) {
  if (
    !req.url.includes(
      "/.well-known/apple-developer-merchantid-domain-association"
    )
  ) {
    next();
    return;
  }

  const slug = req.url.split("/.well-known/")[0].replace("/", "");

  https
    .get(`${process.env.API_URL}/api/quizzes/${slug}/`, (resp) => {
      let data = "";

      // Загружает данные и добавляет в массив.
      resp.on("data", (chunk) => {
        data += chunk;
      });

      // Как поток данных заканчивается, парсит данные и отдает данные файла.
      resp.on("end", () => {
        const quiz = JSON.parse(data);

        try {
          res.writeHead(200, {
            "Content-Type": "application/octet-stream"
          });

          res.end(quiz.data.payment_methods.stripe?.apple_pay_file);
        } catch (e) {
          next();
        }
      });
    })
    .on("error", () => {
      next();
    });
}
