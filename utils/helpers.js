export function findSettingValue(settings, settingName) {
  let result = "";
  settings?.forEach((groupSetting) => {
    groupSetting.forEach((setting) => {
      if (setting.name === settingName) {
        result = setting.value;
      }
    });
  });
  return result;
}

export function findSettingInputValue(settings, settingName) {
  let result = "";
  settings?.forEach((groupSetting) => {
    groupSetting.forEach((setting) => {
      if (setting.name === settingName) {
        result = setting.inputValue;
      }
    });
  });
  return result;
}

export function AmPmTo24h(timeString) {
  let hours = Number(timeString.match(/^(\d+)/)[1]);
  const minutes = Number(timeString.match(/:(\d+)/)[1]);

  const AMPM = timeString.match(/\s(.*)$/)[1];

  if (AMPM === "PM" && hours < 12) hours = hours + 12;
  if (AMPM === "AM" && hours === 12) hours = hours - 12;

  let sHours = hours.toString();
  let sMinutes = minutes.toString();

  if (hours < 10) sHours = "0" + sHours;
  if (minutes < 10) sMinutes = "0" + sMinutes;

  return `${sHours}:${sMinutes}`;
}

export function ToAmPm(time) {
  // Check correct time format and split into components
  time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [
    time
  ];

  if (time.length > 1) {
    // If time format correct
    time = time.slice(1); // Remove full string match value
    time[5] = +time[0] < 12 ? " AM" : " PM"; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }

  if (time[0] < 10) time[0] = "0" + time[0];

  return time.join(""); // return adjusted time or original string
}

export function encodeQueryData(data) {
  const ret = [];
  for (const d in data)
    ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));
  return ret.join("&");
}
