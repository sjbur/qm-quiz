import Vue from "vue";
import themes from "../data/themes.json";

export const state = () => ({
  quiz: {},
  quiz_id: -1,
  screens: [],
  systemScreens: [],
  theme: null,
  themeDark: false,
  themeBackgroundStyle: "color",
  themeBackgroundValue: null,
  result: [],
  preview: false
});

export const mutations = {
  SET_QUIZ(state, { quiz }) {
    Vue.set(state, "quiz", quiz);
  },

  SET_QUIZ_ID(state, { id }) {
    state.quiz_id = id;
  },

  SET_SCREENS(state, { screens, screensKey = "screens" }) {
    Vue.set(state, screensKey, screens);
  },

  SET_THEME(state, { theme }) {
    state.theme = theme;
  },

  SET_THEME_DARKNESS(state, { dark }) {
    state.themeDark = dark;
  },

  SET_THEME_BACKGROUND(state, { style }) {
    state.themeBackgroundStyle = style;
  },

  SET_THEME_BACKGROUND_VALUE(state, { image }) {
    state.themeBackgroundValue = image;
  },

  SET_ANSWER(state, { id, answer }) {
    state.result[id] = answer;
    this.$cookies.set("quiz-result", state.result, {
      path: "/",
      maxAge: 60 * 60
    });
  },

  LOAD_RESULTS_FROM_COOKIE(state) {
    state.result = this.$cookies.get("quiz-result") || [];
  },

  CLEAR_COOKIES(state) {
    state.result = [];
    this.$cookies.removeAll();
  },

  SET_PREVIEW(state, { value }) {
    state.preview = value;
  }
};

export const actions = {
  async fetchQuiz(ctx, { id }) {
    const quiz = await this.$axios
      .$get(`/api/quizzes/${id}/`)
      .then((resp) => resp.data);

    quiz.json_data = JSON.parse(quiz.json_data);

    ctx.commit("SET_QUIZ", { quiz });
    ctx.commit("SET_QUIZ_ID", { id: quiz.id });
    ctx.commit("SET_SCREENS", { screens: quiz.json_data.screens });

    if (quiz.json_data.systemScreens)
      ctx.commit("SET_SCREENS", {
        screens: quiz.json_data.systemScreens,
        screensKey: "systemScreens"
      });

    if (typeof quiz.json_data.theme === "number") {
      ctx.commit("SET_THEME", {
        theme: quiz.json_data.themeDark
          ? themes.dark[quiz.json_data.theme]
          : themes.light[quiz.json_data.theme]
      });
    } else if (typeof quiz.json_data.theme === "object") {
      ctx.commit("SET_THEME", {
        theme: quiz.json_data.themeDark
          ? themes.dark[quiz.json_data.theme.id]
          : themes.light[quiz.json_data.theme.id]
      });
    }
    ctx.commit("SET_THEME_DARKNESS", { dark: quiz.json_data.themeDark });
    ctx.commit("SET_THEME_BACKGROUND", {
      style: quiz.json_data.themeBackgroundStyle
    });

    ctx.commit("SET_THEME_BACKGROUND", {
      style: quiz.json_data.themeBackgroundStyle
    });

    ctx.commit("SET_THEME_BACKGROUND_VALUE", {
      image: quiz.json_data.themeBackgroundValue
    });
  }
};

export const getters = {
  quiz: (state) => state.quiz,
  quiz_id: (state) => state.quiz_id,
  screens: (state) => state.screens,
  theme: (state) => state.theme,
  themeDark: (state) => state.themeDark,
  themeBackgroundStyle: (state) => state.themeBackgroundStyle,
  result: (state) => state.result,
  paymentsExist: (state) => state.quiz?.payment_methods?.stripe,

  themeButtonBackgroundColor: (state) =>
    state.theme?.subscribe?.background || "",

  themeButtonTextColor: (state) => state.theme?.subscribe?.text || "",

  themeBackgroundValue: (state) => state.themeBackgroundValue,

  deeplink: (state) => state.quiz?.deeplink?.deeplink || ""
};
